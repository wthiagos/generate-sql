﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReadFile
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> files = Directory
                .GetFiles("E:\\Downloads\\Base de Ativos\\ALCOA", "Layout_DOR_v1_Alcoa - Todos Beneficiarios.xlsx", SearchOption.AllDirectories)
                .ToList();

            string result = ReadExcel(files);
            // string result = ReadTxt(files);
        }

        public static string ReadTxt(List<string> paths)
        {
            StringBuilder query = new StringBuilder();
            foreach (string path in paths)
            {
                int count = 0, countFile = 2;
                const int BufferSize = 128;
                using (FileStream fileStream = File.OpenRead(path))
                using (StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        if (count == 0)
                        {
                            query.AppendFormat("BEGIN TRAN {0}", Environment.NewLine);
                            count++;
                        }

                        if (!line.Trim().Equals(";") && !line.ToUpper().Contains("INSERT INTO"))
                        {
                            if (line.StartsWith(","))
                                line = line.Replace(",(", "(");
                            query.AppendFormat(
                                "INSERT INTO dor.Endereco (CEP,Logradouro,Endereco,FlagUtilizaTipoLogradouro,CaixaPostalComunitaria,GrandeUsuario,LatitudeEndereco,LongitudeEndereco,Bairro,RegiaoBairro,LatitudeBairro,LongitudeBairro,Localidade,Codigoibge,RegiaoMetropolitanaIBGE,SubdivisaoRegiaoMetropolitanaIBGE,LatitudeLocalidade,LongitudeLocalidade,UF,Estado,Regiaoestado,LatitudeEstado,LongitudeEstado,Pais,LatitudePais,LongitudePais,CodigoCorreios) VALUES ",
                                Environment.NewLine);
                            query.AppendFormat(line + "{0}", Environment.NewLine);
                            count++;
                        }
                        else
                        {
                            if (count < 100000)
                                continue;
                            else
                            {
                                query.AppendFormat("COMMIT", Environment.NewLine);

                                if (!File.Exists("D:\\Teste\\" + countFile.ToString("000") + "_INSERT.sql"))
                                {
                                    using (StreamWriter tw =
                                        new StreamWriter("D:\\Teste\\" + countFile.ToString("000") + "_INSERT.sql",
                                            true))
                                    {
                                        tw.Write(query.ToString());
                                    }
                                }

                                count = 0;
                                countFile++;
                                query = new StringBuilder();
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(query.ToString()))
                    {
                        query.AppendFormat("COMMIT", Environment.NewLine);

                        if (!File.Exists("D:\\Teste\\" + countFile.ToString("000") + "_INSERT.sql"))
                        {
                            using (StreamWriter tw =
                                new StreamWriter("D:\\Teste\\" + countFile.ToString("000") + "_INSERT.sql", true))
                            {
                                tw.Write(query.ToString());
                            }
                        }
                    }
                }
            }

            return query.ToString();
        }

        public static string ReadExcel(List<string> paths)
        {
            StringBuilder query = new StringBuilder();
            foreach (string path in paths)
            {
                FileInfo existingFile = new FileInfo(path);
                using (ExcelPackage package = new ExcelPackage(existingFile))
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets[3];
                    int colCount = worksheet.Dimension.End.Column;
                    int rowCount = worksheet.Dimension.End.Row;

                    //query.Append(CodigoDependente(worksheet, colCount, rowCount).ToString());
                    //query.Append(SAS(worksheet, colCount, rowCount).ToString());
                    //query.Append(RegraElexibilidadeAlcoa(worksheet, colCount, rowCount));
                    //query.Append(AtualizarCNSCNU(worksheet, colCount, rowCount));
                    // query.Append(AtualizarCNSCNUImpeditivos(worksheet, colCount, rowCount));
                    // query.Append(AtualizarCertificadoBradesco(worksheet, colCount, rowCount));
                    query.Append(ValidarChaveBeneficiarioLayoutV1Alcoa(worksheet, colCount, rowCount));
                }
            }

            return query.ToString();
        }

        public static StringBuilder SAS(ExcelWorksheet worksheet, int colCount, int rowCount)
        {
            StringBuilder query = new StringBuilder();

            for (int row = 2; row <= rowCount; row++)
            {
                query.Append("UPDATE PC");
                query.Append(Environment.NewLine);
                query.AppendFormat("SET PC.CodigoProduto = '{0}'", worksheet.Cells[row, 8].Value.ToString().Trim());
                query.Append(Environment.NewLine);
                query.Append("FROM dor.ProdutoContrato PC");
                query.Append(Environment.NewLine);
                query.Append("INNER JOIN dor.Contrato C");
                query.Append(Environment.NewLine);
                query.Append("ON C.IdContrato = PC.IdContrato");
                query.Append(Environment.NewLine);
                query.Append("INNER JOIN dor.DetalhamentoContrato DC");
                query.Append(Environment.NewLine);
                query.Append("ON DC.IdContratoAnuente = C.IdContrato");
                //query.Append(Environment.NewLine);
                //query.Append("INNER JOIN dor.Operadora O");
                //query.Append(Environment.NewLine);
                //query.Append("ON O.IdOperadora = C.IdOperadora");
                query.Append(Environment.NewLine);
                query.Append("INNER JOIN dor.Empresa E");
                query.Append(Environment.NewLine);
                query.Append("ON E.IdEmpresa = C.IdEmpresa");
                query.Append(Environment.NewLine);
                query.AppendFormat("WHERE C.NumeroContrato like '%{0}'",
                    worksheet.Cells[row, 1].Value.ToString().Trim());
                query.Append(Environment.NewLine);
                query.AppendFormat("AND DC.NumeroSubContratoOperadora like '%{0}'",
                    worksheet.Cells[row, 2].Value.ToString().Trim());
                query.Append(Environment.NewLine);
                query.AppendFormat("AND E.Cnpj like '%{0}'",
                    worksheet.Cells[row, 3].Value.ToString().Trim().Replace("'", ""));
                //query.Append(Environment.NewLine);
                //query.AppendFormat("AND O.Cnpj like '%{0}'", worksheet.Cells[row, 4].Value.ToString().Trim().Replace("'", ""));
                query.Append(Environment.NewLine);
                query.Append(Environment.NewLine);

                //////////////////////////////////////////////////////////
                //query.Append("SELECT DISTINCT C.NumeroContrato, O.Cnpj, O.RazaoSocial");
                //query.Append(Environment.NewLine);
                //query.Append("FROM dor.Contrato C");
                //query.Append(Environment.NewLine);
                //query.Append("INNER JOIN dor.DetalhamentoContrato DC");
                //query.Append(Environment.NewLine);
                //query.Append("ON DC.IdContratoAnuente = C.IdContrato");
                //query.Append(Environment.NewLine);
                //query.Append("INNER JOIN dor.Operadora O");
                //query.Append(Environment.NewLine);
                //query.Append("ON O.IdOperadora = C.IdOperadora");
                //query.Append(Environment.NewLine);
                //query.Append("INNER JOIN dor.Empresa E");
                //query.Append(Environment.NewLine);
                //query.Append("ON E.IdEmpresa = C.IdEmpresa");
                //query.Append(Environment.NewLine);
                //query.AppendFormat("WHERE C.NumeroContrato like '%{0}'", worksheet.Cells[row, 1].Value.ToString().Trim());
                //query.Append(Environment.NewLine);
                //query.AppendFormat("AND DC.NumeroSubContratoOperadora like '%{0}'", worksheet.Cells[row, 2].Value.ToString().Trim());
                //query.Append(Environment.NewLine);
                //query.AppendFormat("AND E.Cnpj like '%{0}'", worksheet.Cells[row, 3].Value.ToString().Trim().Replace("'", ""));
                //query.Append(Environment.NewLine);
                //query.Append(Environment.NewLine);
            }

            return query;
        }

        public static string CodigoDependente(ExcelWorksheet worksheet, int colCount, int rowCount)
        {
            StringBuilder query = new StringBuilder();

            for (int row = 2; row <= rowCount; row++)
            {
                query.Append("UPDATE B");
                query.Append(Environment.NewLine);
                query.AppendFormat("SET B.CodigoDependente = '{0}'", worksheet.Cells[row, 10].Value.ToString().Trim());
                query.Append(Environment.NewLine);
                query.Append("FROM dor.Beneficiario B");
                query.Append(Environment.NewLine);
                query.AppendFormat("WHERE B.IdBeneficiario = {0}", worksheet.Cells[row, 1].Value.ToString().Trim());
                query.Append(Environment.NewLine);
                query.Append(Environment.NewLine);
            }

            return query.ToString();
        }

        public static string RegraElexibilidadeAlcoa(ExcelWorksheet worksheet, int colCount, int rowCount)
        {
            StringBuilder query = new StringBuilder();

            for (int row = 2; row <= rowCount; row++)
            {
                query.Append(
                    "INSERT INTO dor.RegraElegibilidadeAlcoa(Localidade, JobBandingMensalista, JobGradeHorista, CodigoSubCliente, Fornecedor, ConteudoPadrao, CodigoFornecedorCodBeneficio, NomePlano, Acomodacao, Abrangencia, CodigoPlanoSistemaFolha, IdLocalSistemaFolha, LocalidadeSistemaFolha)");
                query.Append(Environment.NewLine);
                query.AppendFormat(
                    "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}')",
                    worksheet.Cells[row, 1].Value?.ToString().Trim(),
                    worksheet.Cells[row, 2].Value?.ToString().Trim(),
                    worksheet.Cells[row, 3].Value?.ToString().Trim(),
                    worksheet.Cells[row, 4].Value?.ToString().Trim(),
                    worksheet.Cells[row, 5].Value?.ToString().Trim(),
                    worksheet.Cells[row, 6].Value?.ToString().Trim(),
                    worksheet.Cells[row, 7].Value?.ToString().Trim(),
                    worksheet.Cells[row, 8].Value?.ToString().Trim(),
                    worksheet.Cells[row, 9].Value?.ToString().Trim(),
                    worksheet.Cells[row, 10].Value?.ToString().Trim(),
                    worksheet.Cells[row, 11].Value?.ToString().Trim(),
                    worksheet.Cells[row, 12].Value?.ToString().Trim(),
                    worksheet.Cells[row, 13].Value?.ToString().Trim()
                );
                query.Append(Environment.NewLine);
            }

            return query.ToString();
        }


        //public static string AtualizarCNSCNU(ExcelWorksheet worksheet, int colCount, int rowCount)
        //{
        //    StringBuilder query = new StringBuilder();

        //    query.AppendLine("DECLARE @Beneficiarios TABLE(IdBeneficiario)");
        //    query.AppendFormat("INSERT INTO @Beneficiarios SELECT B.IdBeneficiario FROM dor.Beneficiario B WITH(NOLOCK) INNER JOIN dor.Contrato C WITH(NOLOCK) ON B.IdContrato = C.IdContrato WHERE C.NumeroContrato = '{0}' AND B.NumeroCartao = {1}{2}", Environment.NewLine);

        //    for (int row = 2; row <= rowCount; row++)
        //    {
        //        string contrato = worksheet.Cells[row, 1].Value?.ToString().Trim();
        //        string cartao = worksheet.Cells[row, 2].Value?.ToString().Trim();


        //    }

        //    return query.ToString();
        //}

        public static string AtualizarCNSCNU(ExcelWorksheet worksheet, int colCount, int rowCount)
        {
            StringBuilder query = new StringBuilder();

            for (int row = 2; row <= rowCount; row++)
            {
                string contrato = "'" + worksheet.Cells[row, 1].Value?.ToString().Trim() + "'";
                string cartao = "'" + worksheet.Cells[row, 2].Value?.ToString().Trim() + "'";
                string cns = worksheet.Cells[row, 3].Value?.ToString().Trim();

                if (string.IsNullOrWhiteSpace(cns))
                    cns = "NULL";
                else
                    cns = "'" + cns + "'";

                query.AppendFormat(
                    "UPDATE B SET B.NumeroCNS = {0} FROM dor.Beneficiario B WITH(NOLOCK) INNER JOIN dor.Contrato C WITH(NOLOCK) ON C.IdContrato = B.IdContrato WHERE C.NumeroContrato = {1} AND B.NumeroCartao = {2}",
                    cns, contrato, cartao);
                query.Append(Environment.NewLine);
            }

            return query.ToString();
        }

        public static string AtualizarCNSCNUImpeditivos(ExcelWorksheet worksheet, int colCount, int rowCount)
        {
            StringBuilder query = new StringBuilder();

            for (int row = 2; row <= rowCount; row++)
            {
                string contrato = "'" + worksheet.Cells[row, 1].Value?.ToString().Trim() + "'";
                string cartao = "'" + worksheet.Cells[row, 3].Value?.ToString().Trim() + "'";
                string cns = worksheet.Cells[row, 2].Value?.ToString().Trim();

                if (string.IsNullOrWhiteSpace(cns))
                    cns = "NULL";
                else
                    cns = "'" + cns + "'";


                query.AppendFormat(
                    "UPDATE B SET B.NumeroCNS = {0} FROM stg.MovimentacaoInclusaoGrupoFamiliar B WITH(NOLOCK) INNER JOIN dor.Contrato C WITH(NOLOCK) ON C.IdContrato = B.IdContrato INNER JOIN dor.MovimentacaoArquivo MA WITH(NOLOCK) ON MA.IdMovimentacaoArquivo = B.IdMovimentacaoArquivo WHERE C.NumeroContrato = {1} AND B.NumeroCartao = {2} AND MA.DataInicioMovimentacao >= '2020-05-08 15:06' AND MA.DataInicioMovimentacao <= '2020-05-08 15:07' AND MA.DatafimMovimentacao >= '2020-05-08 15:13' AND MA.DatafimMovimentacao <= '2020-05-08 15:14'",
                    cns, contrato, cartao);
                query.Append(Environment.NewLine);
            }

            return query.ToString();
        }

        public static string AtualizarCertificadoBradesco(ExcelWorksheet worksheet, int colCount, int rowCount)
        {
            StringBuilder query = new StringBuilder();

            for (int row = 4; row <= rowCount; row++)
            {
                string subfatura = worksheet.Cells[row, 2].Value?.ToString().Trim();

                if (!string.IsNullOrWhiteSpace(subfatura))
                    subfatura = int.Parse(subfatura).ToString("0000");

                subfatura = "'" + subfatura + "'";
                string certificado = "'" + worksheet.Cells[row, 3].Value?.ToString().Trim() + "'";
                string cpf = "'" + worksheet.Cells[row, 9].Value?.ToString().Trim() + "'";

                query.AppendFormat(
                    "UPDATE B SET B.NumeroMatriculaOperadora = {0} FROM dor.Beneficiario B WITH(NOLOCK) INNER JOIN dor.DetalhamentoContrato DC WITH(NOLOCK) ON DC.IdDetalhamentoContrato = B.IdDetalhamentoContrato  WHERE B.NumeroCPF = {1} AND DC.NumeroSubContratoOperadora = {2} AND B.NumeroMatriculaOperadora IS NULL AND B.IdMoveStatus IN (27,28) AND B.IdOperadora = 55 AND B.IdEmpresa = 84 AND IdGrauParentesco = 9",
                    certificado, cpf, subfatura);
                query.Append(Environment.NewLine);
            }

            return query.ToString();
        }

        public static string ValidarChaveBeneficiarioLayoutV1Alcoa(ExcelWorksheet worksheet, int colCount, int rowCount)
        {
            StringBuilder query = new StringBuilder();
            List<BeneficiarioAlcoa> beneficiarios = new List<BeneficiarioAlcoa>();

            for (int row = 2; row <= rowCount; row++)
            {
                string operadora = worksheet.Cells[row, 1].Value?.ToString().Trim().ToUpper();
                string contrato = worksheet.Cells[row, 2].Value?.ToString().Trim().ToUpper();
                string matricula = worksheet.Cells[row, 7].Value?.ToString().Trim().ToUpper();
                string nome = worksheet.Cells[row, 10].Value?.ToString().Trim().ToUpper();
                string documento = worksheet.Cells[row, 47].Value?.ToString().Trim().ToUpper();
                string grauParentesco = worksheet.Cells[row, 15].Value?.ToString().Trim().ToUpper();
                string primeiroNome;
                string segundoNome;
                string ultimoNome;

                if (
                    string.IsNullOrWhiteSpace(operadora) ||
                    string.IsNullOrWhiteSpace(contrato) ||
                    string.IsNullOrWhiteSpace(matricula) ||
                    string.IsNullOrWhiteSpace(nome)
                )
                    continue;

                var palavras = nome.Split(' ');
                primeiroNome = palavras.FirstOrDefault();
                segundoNome = palavras.Count() > 1 ? palavras[1] : palavras.LastOrDefault();
                ultimoNome = palavras.LastOrDefault();

                if (long.TryParse(contrato, out var c))
                    contrato = c.ToString();
                
                matricula = Regex.Match(matricula, @"\d+").Value;

                try
                {
                    beneficiarios.Add(new BeneficiarioAlcoa
                    {
                        Linha = row,
                        Operadora = operadora,
                        Contrato = contrato,
                        Matricula = long.Parse(matricula).ToString(),
                        Nome = nome,
                        Documento = documento,
                        PrimeiroNome = primeiroNome,
                        SegundoNome = segundoNome,
                        UltimoNome = ultimoNome,
                        GrauParentesco = grauParentesco
                    });
                }
                catch (Exception error)
                {
                }
            }

            var teste = beneficiarios
                .Where(b => b.GrauParentesco != "TITULAR")
                .GroupBy(b =>
                    new
                    {
                        b.Operadora,
                        b.Contrato,
                        b.Matricula,
                        b.PrimeiroNome,
                        b.SegundoNome,
                        b.UltimoNome
                    })
                .Where(b => b.Count() > 1)
                .ToList();

            return query.ToString();
        }
    }
}

public class BeneficiarioAlcoa
{
    public int Linha { get; set; }
    public string Operadora { get; set; }
    public string Contrato { get; set; }
    public string Matricula { get; set; }
    public string Nome { get; set; }
    public string Documento { get; set; }
    public string PrimeiroNome { get; set; }
    public string SegundoNome { get; set; }
    public string UltimoNome { get; set; }
    public string GrauParentesco { get; set; }
}